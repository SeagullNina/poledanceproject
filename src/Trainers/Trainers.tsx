import React from 'react'
import styles from './Trainers.module.scss'

const Trainers = () =>{
    return(
        <div id="trainers" className={styles.trainers}>
            <span className={styles.headText}>Trainers</span>
            <div className={styles.content}>
            <div className={styles.trainer}>
                <img className={styles.trainImg} src="trainer1.png"/>
                <div className={styles.aboutTrain}>Victoria Glover, 25 years old<br/> Pole Exotic, Pole Trick<br/> Experience as a
                        coach — 5 years<br/><br/> -
                            Participant of World Pole Sport & Fitness Championships 2016<br/> - Participant of IPSF World
                                Pole Sports
                                Championships 2017<br/> - Participant of IPSF World Pole Sports Championships 2018</div>
            </div>
            <div className={styles.trainer}>
                <img className={styles.trainImg} src="trainer2.png"/>
                    <div className={styles.aboutTrain}>Tara Grace, 26 years old<br/> Pole Dance, Pole Exotic<br/> Experience as a coach —
                        7 years<br/><br/>
                            - Participant of IPSF World Pole Sports Championships 2015<br/>
                            - Participant of IPSF World Pole Sports Championships 2017
                    </div>
            </div>
            <div className={styles.trainer}>
                <img className={styles.trainImg} src="trainer3.png"/>
                    <div className={styles.aboutTrain}>Sarah Banks, 24 years old<br/> Pole Dance, Pole Sport <br/>Experience as a coach —
                        5 years<br/><br/>
                            - Participant of World Pole Sport & Fitness Championships 2016 <br/>
                            - Participant of IPSF World Pole Sports Championships 2018</div>
            </div>
            <div className={styles.trainer}>
                <img className={styles.trainImg} src="trainer4.png"/>
                    <div className={styles.aboutTrain}>Zhou Yoon, 24 years old <br/>Pole Sport, Pole Trick <br/>Experience as a coach — 4
                        years<br/><br/>
                            - Participant of World Pole Sport & Fitness Championships 2017<br/>
                            - Participant of IPSF World Pole Sports Championships 2018
                    </div>
            </div>
            </div>
        </div>
    )
}

export default Trainers