import React from "react";
import styles from "./Contacts.module.scss"

const Contacts=()=>{
    return(<div id="contacts" className={styles.contacts}>
        <span className={styles.headText}>Contacts</span>
        <div className={styles.contact}>
            <div className={styles.leftContact}>
                <span className={styles.address}>Address: 1556 Broadway, suite 416, New York<br/><br/>Phone: 8-800-481-51-62<br/><br/>Email: poledancestudio@mail.com<br/><br/></span>
                <div className={styles.buttons}>
                    <button className={styles.web} onClick={()=>window.location.href = 'http://vk.com/'}><img src="vk.png"/></button>
                    <button className={styles.web} onClick={()=>window.location.href = 'http://facebook.com/'}><img src="facebook.png"/></button>
                    <button className={styles.web} onClick={()=>window.location.href =  'http://twitter.com/'}><img src="twitter.png"/></button>
                    <button className={styles.web} onClick={()=>window.location.href =  'http://instagram.com/'}><img src="instagram.png"/></button>
                </div>
                <span className={styles.copyright}>© Pole Dance studio. All right reserved.</span>
            </div>
            <img className={styles.mapImg} src="Map.png"/>
        </div>
    </div>
)
}

export default Contacts