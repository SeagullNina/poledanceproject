import React from 'react'
import styles from './Header.module.scss'

const Header = () =>{
    return(
        <div id="header" className={styles.header}>
            <div className={styles.upHeader}>
                <img className={styles.logo} src="Logo.png"/>
                    <div className={styles.mainMenu}>
                        <div><a href="#about" className={styles.menu}>About us</a></div>
                        <div><a href="#directions" className={styles.menu}>Directions</a></div>
                        <div><a href="#gallery" className={styles.menu}>Gallery</a></div>
                        <div><a href="#trainers" className={styles.menu}>Trainers</a></div>
                        <div><a href="#form" className={styles.menu}>Application forms</a></div>
                        <div><a href="#contacts" className={styles.menu}>Contacts</a></div>
                    </div>
                <div className={styles.number}>8-800-481-51-62</div>
            </div>
            <div className={styles.lowHeader}>
                <div className={styles.headerLeft}>
                    <span className={styles.moto}>Strength, elegance,<br/> grace. It's all
                        <span className={styles.pink}> pole dance</span>.</span>
                    <input className={styles.submit} type="button" value="Submit your application" onClick={()=>{window.location.href = "#form"}}/>
                </div>
                <div className={styles.headerRight}>
                    <img className={styles.mainImg} src="pole.png"/>
                </div>
            </div>
        </div>
    )
}

export default Header