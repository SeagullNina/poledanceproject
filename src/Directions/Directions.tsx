import React from "react";
import styles from './Directions.module.scss'

const Directions = () => {
    return(
        <div id="directions" className={styles.directions}>
            <span className={styles.headText}>Directions</span>
            <div className={styles.direct}>
                <div className={styles.dImg}>
                    <img className={styles.dirImg} src="1.png"/>
                            <div className={styles.header}>Pole Dance</div>
                        <div className={styles.poleInfo}><p>The most feminine and dancing direction. This direction will
                            develop grace and smoothness in you.
                            High heels, sexy hair waving, various flip-flops and rolls, mainly all work in the stalls
                            (near the pylon).</p></div>
                </div>
                <div className={styles.dImg}>
                    <img className={styles.dirImg} src="2.png"/>
                            <div className={styles.header}>Pole Exotic</div>
                        <div className={styles.poleInfo}><p>The most feminine and dancing direction. This direction will
                            develop grace and smoothness in you.<br/>
                            High heels, sexy hair waving, various flip-flops and rolls, mainly all work in the stalls
                            (near the pylon).</p></div>
                </div>
                <div className={styles.dImg}>
                    <img className={styles.dirImg} src="3.png"/>
                            <div className={styles.header}>Pole Sport</div>
                        <div className={styles.poleInfo}><p>The most feminine and dancing direction. This direction will
                            develop grace and smoothness in you.<br/>
                            High heels, sexy hair waving, various flip-flops and rolls, mainly all work in the stalls
                            (near the pylon).</p></div>
                </div>
                <div className={styles.dImg}>
                    <img className={styles.dirImg} src="4.png"/>
                            <div className={styles.header}>Pole Trick</div>
                        <div className={styles.poleInfo}><p>The most feminine and dancing direction. This direction will
                            develop grace and smoothness in you.<br/>
                            High heels, sexy hair waving, various flip-flops and rolls, mainly all work in the stalls
                            (near the pylon).</p></div>
                </div>
            </div>
        </div>
    )
}

    export default Directions