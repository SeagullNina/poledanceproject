import React from 'react';
import './App.css';
import Header from './Header/Header'
import Gallery from './Gallery/Gallery'
import About from './About/About'
import Directions from './Directions/Directions'
import Trainers from './Trainers/Trainers'
import ApplicationForm from "./ApplicationForm/ApplicationForm";
import Contacts from "./Contacts/Contacts";

function App() {
  return (
      <div className="page">
        <Header/>
        <About/>
        <Directions/>
        <Gallery/>
        <Trainers/>
        <ApplicationForm/>
        <Contacts/>
      </div>
  );
}

export default App;
