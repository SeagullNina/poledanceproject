import React from 'react'
import styles from './Gallery.module.scss'

const images = [
    {
        src: "img.png",
        descr: "A movement called “Jamilla split” performed by one of our students"
    },
    {
        src: "img2.png",
        descr: "A movement called “Hook” performed by one of our students"
    },
    {
        src: "img3.png",
        descr: "A movement called “Down” performed by one of our students"
    }
]

const Gallery = () =>{
    const [currentImage, setCurrentImage] = React.useState(0);
    return(
    <div id="gallery" className={styles.gallery}>
    <span className={styles.headText}>Gallery</span>
    <div className={styles.gal}>
        <div className={styles.photos}>
            <img alt="" src={images[currentImage].src} className={styles.galImg}/>
        </div>
        <div className={styles.leftPage}>
            <div className={styles.counter}>{currentImage+1}/{images.length}</div>
            <div className={styles.description}>{images[currentImage].descr}</div>
            <div className={styles.butts}>
            <img alt="" src="Left.png" className={styles.prevAndNext} onClick={() =>{
            if (currentImage === 0) setCurrentImage(images.length - 1)
                else
                    setCurrentImage(currentImage - 1);
            }}/>
            <img alt="" src="Right.png"className={styles.prevAndNext} onClick={()=>{
                if (currentImage === images.length - 1) setCurrentImage(0)
                else
                    setCurrentImage(currentImage + 1)
            }}/>
        </div>
        </div>
    </div>
    </div>
        )
    }

export default Gallery