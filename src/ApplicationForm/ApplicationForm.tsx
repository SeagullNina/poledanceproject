import React from "react";
import styles from "./ApplicationForm.module.scss"

const ApplicationForm = () =>{
    return(
        <div id="form" className={styles.form}>
            <span className={styles.headText}>Application form</span>
            <div className={styles.formContent}>
                <div className={styles.applicationLeft}>
                    <input type="text" className={styles.inputText} placeholder="Name"/>
                        <input type="text" className={styles.inputText} placeholder="Second name"/>
                            <input type="text" className={styles.inputText} placeholder="Email"/>
                                <input type="text" className={styles.inputText} placeholder="Phone"/>
                                    <input className={styles.submit} type="button" value="Submit your application"/>
                </div>
                <img className={styles.appImg} src="imgForm.png"/>
            </div>
        </div>

    )
}

export default ApplicationForm