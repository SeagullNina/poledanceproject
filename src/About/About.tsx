import React from 'react'
import styles from './About.module.scss'

const About = () =>{
    return(
        <div id="about" className={styles.about}>
            <span className={styles.headText}>About us</span>
            <div className={styles.cards}>
                <div className={styles.card}>
                    <img className={styles.aboutImg} src="calendar.png" alt=""/>
                        <span className={styles.mark}>Classes are held every day, so you can choose a time convenient for you</span>
                </div>
                <div className={styles.card}>
                    <img className={styles.aboutImg} src="team.png"alt=""/>
                        <span className={styles.mark}>Trainers deal with small groups of 6 people</span>
                </div>
                <div className={styles.card}>
                    <img className={styles.aboutImg} src="checklist.png"alt=""/>
                        <span className={styles.mark}>Skillfully selected programs are <br/> suitable for both experienced athletes and beginners </span>
                </div>
            </div>
        </div>
    )
}

export default About